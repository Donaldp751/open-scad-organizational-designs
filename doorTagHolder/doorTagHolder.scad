card_width = 93; // [25:350]
card_height = 43; // [10:100]
card_thickness = .25; // [.05:.025:1.2]

mounting_hole_count = 3;
mounting_hole_diameter = 3.5;

card_outline_buffer_zn = 2.5; // [1.0:.25:5]
card_thickness_buffer_zn = .55; // [.2:.05:1.5]

edge_thickness = 2.5; // [2: .25: 5]

card_visual_ratio = .95; // [.5: .01: .95] 


generate_card_holder(card_width, card_height, card_thickness + card_thickness_buffer_zn);



module generate_card_holder(c_width, c_height, c_thickness)
{
 difference(){
 translate([0,0,-(edge_thickness + card_thickness_buffer_zn)]){
    translate([0,0,-(card_thickness + card_thickness_buffer_zn)])
    {
        translate([0,0,-edge_thickness]){
            linear_extrude(edge_thickness){
                square([c_width + card_outline_buffer_zn + edge_thickness, c_height + card_outline_buffer_zn + edge_thickness], center = true);
                }
            }
    difference(){
        linear_extrude(card_thickness + card_thickness_buffer_zn){
                difference(){
                    square([c_width + card_outline_buffer_zn + edge_thickness, c_height + card_outline_buffer_zn + edge_thickness], center = true);
                    square([(c_width + card_outline_buffer_zn), c_height + card_outline_buffer_zn], center = true);
                    }   
                }
        linear_extrude(card_thickness + card_thickness_buffer_zn){
            translate([0, edge_thickness,0]){
                    square([(c_width + card_outline_buffer_zn), c_height + card_outline_buffer_zn], center = true);
                    }
                }
            }
        }
    linear_extrude(card_thickness + card_thickness_buffer_zn + edge_thickness){
        difference(){
            square([c_width + card_outline_buffer_zn + edge_thickness, c_height +            card_outline_buffer_zn + edge_thickness], center = true);
            square([(c_width * card_visual_ratio), c_height * card_visual_ratio], center = true);
            }   
        }
    }
   mounting_holes(mounting_hole_count,mounting_hole_diameter,c_width * card_visual_ratio * .8);
}
}

module mounting_holes(hole_count = 3, hole_diameter=5, hole_max_dist=60)
{
    holeSpacing =  hole_max_dist / (hole_count - 1);
    
    for(i = [0:1:hole_count-1])
    {
     translate([(holeSpacing * i) - hole_max_dist/2, 0, -(card_outline_buffer_zn * 5)])
       {
          linear_extrude(edge_thickness * 5){
          circle(d=hole_diameter, $fn=100);
           }
          translate([0,0,edge_thickness*3]){
           linear_extrude(edge_thickness * 1.5){
          circle(d=hole_diameter*2, $fn=100);
           }
       }
       } 
        
    }
}